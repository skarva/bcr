extends KinematicBody

var device_type = 0
var device_id = 0

const GRAVITY = -9.8
var timer = 0
var max_speed = 20
var jump_speed = 10
var respawn_time = 0
var vel = Vector3()
var is_dead = false

var globals

func _ready():
	globals = get_node("/root/global")
	$MeshInstance.visible = false
	$CollisionShape.disabled = true
	respawn_time = globals.player_respawn_time
	is_dead = true
	pass	

func _unhandled_input(event):
	if !is_dead:
		var event_type = globals.device_type.GAMEPAD
		if event is InputEventKey:
			event_type = globals.device_type.KEY
			
		if is_on_floor():
			vel.x = 0
			vel.z = 0
			
			if event.device == device_id and event_type == device_type:
				if Input.is_action_pressed("away"):
					vel.z = -max_speed
				if Input.is_action_pressed("toward"):
					vel.z = max_speed
				if Input.is_action_pressed("left"):
					vel.x = -max_speed
				if Input.is_action_pressed("right"):
					vel.x = max_speed
				if Input.is_action_pressed("jump"):
					vel.y = jump_speed
		
		if Input.is_action_pressed("ui_cancel"):
			if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _physics_process(delta):
	if !is_dead:
		process_movement(delta)
		
#	process_UI(delta)
	process_respawn(delta)
	
func process_movement(delta):
	vel.y += delta * GRAVITY
	
	move_and_slide(vel, Vector3(0, 1, 0))
	
func process_respawn(delta):
	if timer <= 0 and !is_dead:
		$MeshInstance.visible = false
		$CollisionShape.disabled = true
		respawn_time = globals.player_respawn_time
		is_dead = true
		
	if is_dead:
		respawn_time -= delta
		
		if respawn_time <= 0:
			global_transform.origin = Vector3(0, 0, 0)
			$MeshInstance.visible = true
			$CollisionShape.disabled = false
			timer = globals.bomb_timer
			is_dead = false
	
func explode():
	# BLOW UP
	pass
	
func collide_safety_pin():
	# Timer freeze
	pass