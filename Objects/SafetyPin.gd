extends RigidBody

var count = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	count += delta * 10
	get_child(2).light_energy = sin(count) * 0.5 + 0.5
	
	if Input.is_action_pressed("reset_safety_pin"):
		translation = Vector3(0,0,0)