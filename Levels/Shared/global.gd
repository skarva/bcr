extends Node

var local = true

enum device_type {KEY, GAMEPAD}
enum player_materials {RED, BLUE, YELLOW, GREEN}
var player_device_type = []
var player_device_id = []

# Respawn time in secs
var player_respawn_time = 3
var safetypin_respawn_time = 5

# Game mode vars
var min_players = 1
var bomb_timer = 60