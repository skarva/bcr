extends Spatial

export (PackedScene) var Player
export (float) var rotate_speed = 4

var rotate_left = false
var rotate_right = false
var target
var current

var globals

func _ready():
	globals = get_node("/root/global")
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	load_players()
	pass

func _process(delta):
	if not rotate_left and not rotate_right:
		if Input.is_action_pressed("chamber_rotate_left"):
			rotate_left()
		if Input.is_action_pressed("chamber_rotate_right"):
			rotate_right()
	elif rotate_left:
		current += (delta * rotate_speed)
		rotate_z(delta * rotate_speed)
		if (PI/2) - abs(current) < 0.02:
			rotation.z = target
			rotate_left = false
	elif rotate_right:
		current -= (delta * rotate_speed)
		rotate_z(-delta * rotate_speed)
		if (PI/2) - abs(current) < 0.02:
			rotation.z = target
			rotate_right = false
			
func rotate_left():
	rotate_left = true
	target = rotation.z + (PI/2)
	current = 0
	
func rotate_right():
	rotate_right = true
	target = rotation.z - (PI/2)
	current = 0
	
func load_players():
	for i in range(globals.player_device_id.size()):
		var new_player = Player.instance()
		new_player.device_id = globals.player_device_id[i]
		new_player.device_type = globals.player_device_type[i]
		add_child(new_player)