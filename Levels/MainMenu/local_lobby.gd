extends CenterContainer

var current_player_number = 1

var globals

func _ready():
	globals = get_node("/root/global")

func _input(event):
	if event.is_action_pressed("jump"):
		var type = globals.device_type.GAMEPAD
		if event is InputEventKey:
			type = globals.device_type.KEY
		
		var exist_index = globals.player_device_id.find(event.device)
		if exist_index == -1:
			globals.player_device_type.append(type)
			globals.player_device_id.append(event.device)
			player_ready()
		elif exist_index > -1:
			if globals.player_device_type[exist_index] != type:
				globals.player_device_type.append(type)
				globals.player_device_id.append(event.device)
				player_ready()
		
func player_ready():
	var label = get_node("GridContainer/Player " + String(current_player_number) + "/Join Label")
	label.text = "Ready!"
	current_player_number += 1
	if globals.player_device_id.size() >= globals.min_players:
		get_node("GridContainer/Play").disabled = false